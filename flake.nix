{
  description = "Flake Templates";

  inputs.devshell.url = "github:numtide/devshell";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-24.05";

  outputs = { self, flake-utils, devshell, nixpkgs }:
    flake-utils.lib.eachDefaultSystem (system: {
      devShell =
        let pkgs = import nixpkgs {
          inherit system;

          overlays = [ devshell.overlay ];
        };
        in
        pkgs.devshell.mkShell {
          imports = [ (pkgs.devshell.importTOML ./devshell.toml) ];
        };
    }) // {
      templates = {
        devshell = {
          path = ./templates/devshell;
          description = "A basic flake template with devshell and direnv configured.";
        };
        devshell-python = {
          path = ./templates/devshell-python;
          description = "A flake template for a python project with devshell and direnv.";
        };
      };
    };
}
